    public class BankAccount {
        private int balance = 0;

        public int getBalance() {
            System.out.println("Your bag of holding has a current balance of " + balance);
            return balance;
        }

        public void setBalance(int balance) {
            if ((this.balance + balance) >= 0) {
                this.balance += balance;
                System.out.println("Your bag of holding is changed by " + balance + " and now has a total balance of: " + this.balance);
            } else {
                System.out.println("You reach in to withdraw " + balance + " however you fail to find that amount. You are left holding " + this.balance);
            }
        }
    }

